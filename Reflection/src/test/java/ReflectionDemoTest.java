import org.junit.Test;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import static org.junit.Assert.*;

public class ReflectionDemoTest {
    @Test
    public void testCountOfClassesWithTypeHumanOrDerived(){
        ReflectionDemo reflectionDemo = new ReflectionDemo();

        List<Object> list = new ArrayList<>();
        Human human1 = new Human("", "", "", 0);
        Human human2 = new Human("", "", "", 0);
        Student student = new Student("", "", "", 0, "");
        Executable executable1 = new Executable();
        Executable executable2 = new Executable();
        list.add(human1);
        list.add(human2);
        list.add(student);
        list.add(executable1);
        list.add(executable2);

        assertEquals(3, reflectionDemo.countOfClassesWithTypeHumanOrDerived(list));
    }

    @Test
    public void testGetListOfPublicMethods(){
        ReflectionDemo reflectionDemo = new ReflectionDemo();

        Human human = new Human("", "", "", 0);
        List<String> list = new ArrayList<>();
        list.add("getSurname");
        list.add("getName");
        list.add("getPatronymic");
        list.add("getAge");
        list.add("setSurname");
        list.add("setName");
        list.add("setPatronymic");
        list.add("setAge");
        list.add("toString");
        list.add("equals");
        list.add("hashCode");
        list.add("wait");
        list.add("wait");
        list.add("wait");
        list.add("getClass");
        list.add("notify");
        list.add("notifyAll");

        List<String> listExcepted = reflectionDemo.getListOfPublicMethods(human);
        Collections.sort(listExcepted);
        Collections.sort(list);

        assertEquals(list, listExcepted);
    }

    @Test
    public void testGetListOfSuperclasses(){
        ReflectionDemo reflectionDemo = new ReflectionDemo();

        Student student = new Student("", "", "", 0, "");
        List<String> list = new ArrayList<>();
        list.add("Human");
        list.add("java.lang.Object");

        assertEquals(list, reflectionDemo.getListOfSuperclasses(student));
    }

    @Test
    public void testCountOfObjectsWithMethodExecute() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        ReflectionDemo reflectionDemo = new ReflectionDemo();

        List<Object> list = new ArrayList<>();
        Human human1 = new Human("", "", "", 0);
        Human human2 = new Human("", "", "", 0);
        Student student = new Student("", "", "", 0, "");
        Executable executable1 = new Executable();
        Executable executable2 = new Executable();
        list.add(human1);
        list.add(human2);
        list.add(student);
        list.add(executable1);
        list.add(executable2);

        assertEquals(2, reflectionDemo.countOfObjectsWithMethodExecute(list));
    }

    @Test
    public void testCountOfObjectsWithMethodExecute2() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        ReflectionDemo reflectionDemo = new ReflectionDemo();

        List<Object> list = new ArrayList<>();
        Human human1 = new Human("", "", "", 0);
        Human human2 = new Human("", "", "", 0);
        Student student = new Student("", "", "", 0, "");
        Executable executable1 = new Executable();
        Executable executable2 = new Executable();
        list.add(human1);
        list.add(human2);
        list.add(student);
        list.add(executable1);
        list.add(executable2);
        list.add(new BadClass());

        assertEquals(3, reflectionDemo.countOfObjectsWithMethodExecute(list));
    }

    @Test
    public void testGetListOfGettersAndSetters(){
        ReflectionDemo reflectionDemo = new ReflectionDemo();

        Human human = new Human("", "", "", 0);
        List<String> list = new ArrayList<>();
        list.add("getSurname");
        list.add("getName");
        list.add("getPatronymic");
        list.add("getAge");
        list.add("setSurname");
        list.add("setName");
        list.add("setPatronymic");
        list.add("setAge");
        list.add("getClass");

        List<String> listExcepted = reflectionDemo.getListOfGettersAndSetters(human);
        Collections.sort(listExcepted);
        Collections.sort(list);

        assertEquals(list, listExcepted);
    }

    @Test
    public void testGetListOfGettersAndSetters2(){
        ReflectionDemo reflectionDemo = new ReflectionDemo();

        List<String> list = new ArrayList<>();
        list.add("getA");
        list.add("setA");
        list.add("getClass");

        List<String> listExcepted = reflectionDemo.getListOfGettersAndSetters(new BadClass());
        Collections.sort(listExcepted);
        Collections.sort(list);

        assertEquals(list, listExcepted);
    }

}