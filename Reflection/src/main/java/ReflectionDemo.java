import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

public class ReflectionDemo {
    public int countOfClassesWithTypeHumanOrDerived(List<Object> list){
        int count = 0;
        for(Object obj : list){
            if(obj instanceof Human){
                count++;
            }
        }
        return count;
    }
    public List getListOfPublicMethods(Object obj){
        List<String> methodList = new ArrayList<>();
        for(Method method : obj.getClass().getMethods()){
            /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
             Бесполезная проверка. Читаем документацию getMethods
            Returns an array containing Method objects reflecting all the public methods of the class or interface
            represented by this Class object, including those declared by the class or interface and those inherited
            from superclasses and superinterfaces.
            */
            methodList.add(method.getName());
        }
        return methodList;
    }
    public List getListOfSuperclasses(Object obj){
        List<String> classList = new ArrayList<>();
        Class<?> objClass = obj.getClass();
        while (objClass.getSuperclass() != null){
            classList.add(objClass.getSuperclass().getName());
            objClass = objClass.getSuperclass();
        }
        return classList;
    }

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! См.тест.
    */
    public int countOfObjectsWithMethodExecute(List<Object> objectList) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        int count = 0;
        for (Object obj : objectList){
            if (obj instanceof IExecutable) {
                obj.getClass().getMethod("execute").invoke(obj, obj.getClass().getMethod("execute").getTypeParameters());
                count++;
            }
        }
        return count;
    }

    public List getListOfGettersAndSetters(Object obj){
        List<String> list = new ArrayList<>();
        for (Method method : obj.getClass().getMethods()){
            if(Modifier.isPublic(method.getModifiers()) && !Modifier.isStatic(method.getModifiers()) &&
                    !method.getReturnType().getName().equals("void") && method.getParameterCount() == 0 &&
                    method.getName().startsWith("get")){
                list.add(method.getName());
            }
            if(Modifier.isPublic(method.getModifiers()) && !Modifier.isStatic(method.getModifiers()) &&
                    method.getReturnType().getName().equals("void") && method.getParameterCount() == 1 &&
                    method.getName().startsWith("set")){
                list.add(method.getName());
            }
        }
        return list;
    }
}
